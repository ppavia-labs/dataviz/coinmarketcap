package ppa.labs.coinmarketcap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinMarketCapApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinMarketCapApplication.class, args);
	}
}
