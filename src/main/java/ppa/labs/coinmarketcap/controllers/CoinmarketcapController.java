package ppa.labs.coinmarketcap.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ppa.labs.coinmarketcap.services.CoinmarketcapService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/coinmarketcap")
public class CoinmarketcapController {
	private static final Logger logger = LoggerFactory.getLogger(CoinmarketcapController.class);

	@Autowired
	private CoinmarketcapService coinmarketcapService;
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/root"
			)
	public String getMarketCap () {
		String response = "";
		String uri = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest";
	    List<NameValuePair> paratmers = new ArrayList<NameValuePair>();
	    paratmers.add(new BasicNameValuePair("start","1"));
	    paratmers.add(new BasicNameValuePair("limit","10"));
	    paratmers.add(new BasicNameValuePair("convert","USD"));
	    try {
	    	response = coinmarketcapService.makeAPICall(uri, paratmers);
	    } catch (IOException e) {
	    	logger.error("Error: cannont access content - " + e.toString());
	    } catch (URISyntaxException e) {
	    	logger.error("Error: Invalid URL " + e.toString());
	    }
		return response;
	}
}
