package ppa.labs.coinmarketcap.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ppa.labs.coinmarketcap.services.CoinmarketcapService;
import ppa.labs.coinmarketcap.services.CoinmarketcapServiceImpl;


@Configuration
public class CoinmarketcapConfiguration {
	@Bean
	public CoinmarketcapService coinmarketcapService () {
		return new CoinmarketcapServiceImpl();
	}
}
