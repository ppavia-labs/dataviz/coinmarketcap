package ppa.labs.coinmarketcap.services;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.NameValuePair;

public interface CoinmarketcapService {
	public String makeAPICall(String uri, List<NameValuePair> parameters) throws URISyntaxException, IOException;
}
